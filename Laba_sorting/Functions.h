#pragma once
#include <stdio.h>
#include <iostream>
#include <time.h>

using namespace std;

void LinearSort(int* arr, int size);
void BubbleSort(int* arr, int size);
void InsertalSort(int* arr, int size);

int LinearSearch(int* arr, int size, int value);
int BarierSearch(int* arr, int size, int value);
int BinarySearch(int* arr, int size, int value);

void PrintArr(int* arr, int size);
void RandInit(int* arr, int size);


int Dialoge(int* arr, int maxsize);

int ArrayInput(int* arr, int maxsize);
void CompareSorts();




