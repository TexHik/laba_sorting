#include "Functions.h"


void LinearSort(int* arr, int size) {
	int j = 0;
	int buff = 0;
	for (int i = 0; i < size; i++) {
		j = i;
		for (int k = i; k < size; k++) {
			if (arr[j] > arr[k]) {
				j = k;
			}
		}
		buff = arr[i];
		arr[i] = arr[j];
		arr[j] = buff;
	}
}

void BubbleSort(int* arr, int size) {
	int tmp = 0;
	for (int i = 0; i < size; i++) {
		for (int j = (size - 1); j >= (i + 1); j--) {
			if (arr[j] < arr[j - 1]) {
				tmp = arr[j];
				arr[j] = arr[j - 1];
				arr[j - 1] = tmp;
			}
		}
	}
}

void InsertalSort(int* arr, int size) {
	int key = 0;
	int i = 0;
	for (int j = 1; j < size; j++) {
		key = arr[j];
		i = j - 1;
		while (i >= 0 && arr[i] > key) {
			arr[i + 1] = arr[i];
			i = i - 1;
			arr[i + 1] = key;
		}
	}
}


int LinearSearch(int* arr, int size, int value) {
	for (int i = 0; i < size; i++) {
		if (arr[i] == value) {
			return i;
		}
	}
	return -1;
}

int BarierSearch(int* arr, int size, int value) {
	int i = 0;
	if (arr[size] == value) return size;
	arr[size] = value;
	for (i = 0; arr[i] != value; i++);
	if (i == size) return -1;
	return i;
}

int BinarySearch(int* arr, int size, int value) {
	int left = 0, right = size, midd = 0;
	while (true)
	{
		midd = (left + right) / 2;
		if (value < arr[midd]) { right = midd - 1; } 
		else if (value > arr[midd]) {left = midd + 1;} 
		else return midd;         

		if (left > right)
			return -1;
	}
}


void PrintArr(int* arr, int size) {
	bool overed = false;
	if (size > 20) {size = 20; overed = true;}

	for (int i = 0; i < size; i++) {
		printf("%d ", arr[i]);
	}
	if (overed) printf(".....");
	printf("\n");
}

void RandInit(int* arr, int size) {
	for (int i = 0; i < size; i++) {
		arr[i] = -100 + rand() % 200;
	}
}





int ArrayInput(int* arr, int maxsize) {
	int size = 0, curlen = 0;
	int buff;
	do {
		printf("Enter array length(1-20 manual/20-%d random): ", maxsize);
		scanf_s("%d", &size);
	} while (size <= 0 || size > maxsize);
	if (size < 20) {
		printf("Enter numbers: \n");
		do {
			printf("[%d/%d]: ", curlen + 1, size);
			scanf_s("%d", &buff);
			arr[curlen] = buff;
			curlen++;
		} while (curlen < size);
	}
	else {
		RandInit(arr, size);
		curlen = size;
	}
	return curlen;
}
void CompareSorts() {
	const int arrsize = 7000;
	int origin[arrsize];
	RandInit(origin, arrsize);
	int a[arrsize];
	 clock_t start, end;
	__int64 mstime = 0;
	printf("\n");
#pragma region BubbleSort
	memcpy(a, origin, sizeof(int) * arrsize);
	start = clock();
	BubbleSort(a, arrsize);
	end = clock();

	mstime = (int)(1000 * (double)(end - start) / CLOCKS_PER_SEC);
	printf("Bubble sort sort work time: %I64dms.\n", mstime);
#pragma endregion	
	printf("\n");
#pragma region LinearSort
	memcpy(a, origin, sizeof(int) * arrsize);
	start = clock();
	LinearSort(a, arrsize);
	end = clock();

	mstime = (int)(1000 * (double)(end - start) / CLOCKS_PER_SEC);
	printf("Linear sort work time: %I64dms.\n", mstime);
#pragma endregion
	printf("\n");
#pragma region InsertalSort
	memcpy(a, origin, sizeof(int) * arrsize);
	start = clock();
	InsertalSort(a, arrsize);
	end = clock();

	mstime = (int)(1000 * (double)(end - start) /CLOCKS_PER_SEC);
	printf("Insertal sort work time:%I64dms.\n", mstime);
#pragma endregion
	printf("\n");
}

int Dialoge(int *arr, int maxsize) {
	bool sorted = false;
	int size = 0;
	while (true) {
		int choose;
		printf("\nSelect: \n\
		\r1.Input array\n\
		\r2.Linear sort\n\
		\r3.Bubble sort\n\
		\r4.Insertal sort\n\
		\r5.Linear search\n\
		\r6.Binary search\n\
		\r7.Compare alghorithms\n\
		\r8.Print array\n");
		scanf_s("%d", &choose);
		system("cls");
		int val = 0;
		switch (choose)
		{
		case 1:
			size = ArrayInput(arr, maxsize);
			sorted = false;
			break;
		case 2:
			LinearSort(arr, size);
			printf("Sorted\n");
			sorted = true;
			break;
		case 3:
			BubbleSort(arr, size);
			printf("Sorted\n");
			sorted = true;
			break;
		case 4:
			InsertalSort(arr, size);
			printf("Sorted\n");
			sorted = true;
			break;
		case 5:
			printf("Enter int value to look for: ");
			scanf_s("%d", &val);
			if (val == -1) {
				printf("Not found");
			}
			else {
				printf("Value %d found on pos: %d", val, LinearSearch(arr, size, val));
			}
			break;
		case 6:
			if (!sorted) {
				printf("Cant find, Array isn't sorted!\n");
				break;
			}
			printf("Enter int value to look for: ");
			scanf_s("%d", &val);
			if (val == -1) {
				printf("Not found");
			}
			else {
				printf("Value %d found on pos: %d", val, BinarySearch(arr, size, val));
			}
			break;
		case 7:
			CompareSorts();
			break;
		case 8:
			PrintArr(arr, size);
			break;
		default:
			printf("Not existing selector");
			break;
		}



	}
}